package fyber.challenge.one

import java.io.FileNotFoundException

import scala.collection.mutable.ListBuffer
import scala.io.Source

/**
  * Created by JuanManuel on 7/13/2017.
  */
object TSeries {

  def main(args: Array[String]): Unit = {
    if (args.length < 2) {
      System.err.println("Usage: TimeSeries <filePath> <windowLength>")
      System.exit(1)
    }
    val filePath = args(0)
    try {
      val windowLength = args(1).toInt
      performAnalysis(filePath, windowLength)
    } catch {
      case ex: NumberFormatException =>{
        System.err.println("The rolling time window must be an integer value")
        System.exit(1)
      }
    }
  }

  def performAnalysis(filePath: String, windowLength: Int): String = {
    try {
      val file = Source.fromFile(filePath)
      val iter = file.getLines()
      if (iter.isEmpty)
        return "The input file is empty"

      println("T          V       N RS      MinV    MaxV\n--------------------------------------------")
      val previousLines = new ListBuffer[(Long, Double)]()

      while (!iter.isEmpty) {
        val line = iter.next().split("\t")
        val timestamp = line(0).toLong
        val ratio = line(1).toDouble
        val result = analyseAtTimeT(timestamp, ratio, windowLength, previousLines)
        println(result._1.mkString)
        previousLines += ((timestamp, ratio))

        val j = result._2
        if (j > -1) {
          previousLines.remove(0, j+1)
        }
      }
      return "Analysis completed successfully"
    } catch {
      case ex: FileNotFoundException =>{
        return "Missing input file"
      }
    }
  }

  def analyseAtTimeT(timestamp: Long, ratio: Double, windowLength: Int,
                     previousLines: ListBuffer[(Long, Double)]): (WinAnalysis, Int) = {
    var min = ratio
    var max = ratio
    var sum = ratio
    var count = 1
    var j = previousLines.length-1

    while (j >= 0 && (timestamp - previousLines(j)._1 < windowLength)) {
      val prevRatio = previousLines(j)._2
      count = count+1
      if (prevRatio < min)
        min = prevRatio
      if (prevRatio > max)
        max = prevRatio
      sum += prevRatio
      j -= 1
    }
    return (new WinAnalysis(timestamp, ratio, count, sum, min, max), j)
  }
}