package fyber.challenge.one

/**
  * Created by juan211189 on 13.07.17.
  */
class WinAnalysis(val windowEnd: Long, val ratio: Double, val nMeasurements: Int,
                  val rollingSum: Double, val minV: Double, val maxV: Double) {

  def mkString: String = {
    return windowEnd+" "+ratio+" "+nMeasurements+" "+
      BigDecimal(rollingSum).setScale(5, BigDecimal.RoundingMode.HALF_UP)+" "+minV+" "+maxV
  }
}
