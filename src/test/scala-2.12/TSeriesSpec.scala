import fyber.challenge.one.TSeries

import collection.mutable.ListBuffer
import org.scalatest.FlatSpec

/**
  * Created by JuanManuel on 7/12/2017.
  */
class TSeriesSpec extends FlatSpec {

  "An analysis" should "indicate when the file cannot be found" in {
    assert(TSeries.performAnalysis("wrong/path", 60) === "Missing input file")
  }

  it should "indicate when the file is empty" in {
    assert(TSeries.performAnalysis(getClass.getResource("empty_scala.txt").getPath, 60) === "The input file is empty")
  }

  it should "work properly with a single line file" in {
    val previousLines = new ListBuffer[(Long, Double)]()
    val result = TSeries.analyseAtTimeT(1355270609, 1.80215, 60, previousLines)._1.mkString
    assert(result === "1355270609 1.80215 1 1.80215 1.80215 1.80215")
  }

  it should "work properly with the same two lines" in {
    val previousLines = new ListBuffer[(Long, Double)]()
    previousLines += ((1355270609, 1.80215))
    val result = TSeries.analyseAtTimeT(1355270609, 1.80215, 60, previousLines)._1.mkString
    assert(result === "1355270609 1.80215 2 3.60430 1.80215 1.80215")
  }

  it should "work properly at 2nd line time in the given example" in {
    val previousLines = new ListBuffer[(Long, Double)]()
    previousLines += ((1355270609, 1.80215))
    val result = TSeries.analyseAtTimeT(1355270621, 1.80185, 60, previousLines)._1.mkString
    assert(result === "1355270621 1.80185 2 3.60400 1.80185 1.80215")
  }

  it should "work properly at 3rd line time in the given example" in {
    val previousLines = new ListBuffer[(Long, Double)]()
    previousLines += ((1355270609, 1.80215))
    previousLines += ((1355270621, 1.80185))
    val result = TSeries.analyseAtTimeT(1355270646, 1.80195, 60, previousLines)._1.mkString
    assert(result === "1355270646 1.80195 3 5.40595 1.80185 1.80215")
  }

  it should "work properly at 4rd line time in the given example" in {
    val previousLines = new ListBuffer[(Long, Double)]()
    previousLines += ((1355270609, 1.80215))
    previousLines += ((1355270621, 1.80185))
    previousLines += ((1355270646, 1.80195))
    val result = TSeries.analyseAtTimeT(1355270702 , 1.80225, 60, previousLines)._1.mkString
    assert(result === "1355270702 1.80225 2 3.60420 1.80195 1.80225")
  }

  it should "work properly at 5th line time in the given example" in {
    val previousLines = new ListBuffer[(Long, Double)]()
    previousLines += ((1355270609, 1.80215))
    previousLines += ((1355270621, 1.80185))
    previousLines += ((1355270646, 1.80195))
    previousLines += ((1355270702, 1.80225))
    val result = TSeries.analyseAtTimeT(1355270702, 1.80215, 60, previousLines)._1.mkString
    assert(result === "1355270702 1.80215 3 5.40635 1.80195 1.80225")
  }

  it should "work properly at 10th line time in the given example" in {
    val previousLines = new ListBuffer[(Long, Double)]()
    previousLines += ((1355270609, 1.80215))
    previousLines += ((1355270621, 1.80185))
    previousLines += ((1355270646, 1.80195))
    previousLines += ((1355270702, 1.80225))
    previousLines += ((1355270702, 1.80215))
    previousLines += ((1355270829, 1.80235))
    previousLines += ((1355270854, 1.80205))
    previousLines += ((1355270868, 1.80225))
    previousLines += ((1355271000, 1.80245))
    val result = TSeries.analyseAtTimeT(1355271023, 1.80285, 60, previousLines)._1.mkString
    assert(result === "1355271023 1.80285 2 3.60530 1.80245 1.80285")
  }

  it should "work properly at 15th line time in the given example" in {
    val previousLines = new ListBuffer[(Long, Double)]()
    previousLines += ((1355270609, 1.80215))
    previousLines += ((1355270621, 1.80185))
    previousLines += ((1355270646, 1.80195))
    previousLines += ((1355270702, 1.80225))
    previousLines += ((1355270702, 1.80215))
    previousLines += ((1355270829, 1.80235))
    previousLines += ((1355270854, 1.80205))
    previousLines += ((1355270868, 1.80225))
    previousLines += ((1355271000, 1.80245))
    previousLines += ((1355271023, 1.80285))
    previousLines += ((1355271024, 1.80275))
    previousLines += ((1355271026, 1.80285))
    previousLines += ((1355271027, 1.80265))
    previousLines += ((1355271056, 1.80275))
    val result = TSeries.analyseAtTimeT(1355271428, 1.80265, 60, previousLines)._1.mkString
    assert(result === "1355271428 1.80265 1 1.80265 1.80265 1.80265")
  }

  it should "work properly at 20th line time in the given example" in {
    val previousLines = new ListBuffer[(Long, Double)]()
    previousLines += ((1355270609, 1.80215))
    previousLines += ((1355270621, 1.80185))
    previousLines += ((1355270646, 1.80195))
    previousLines += ((1355270702, 1.80225))
    previousLines += ((1355270702, 1.80215))
    previousLines += ((1355270829, 1.80235))
    previousLines += ((1355270854, 1.80205))
    previousLines += ((1355270868, 1.80225))
    previousLines += ((1355271000, 1.80245))
    previousLines += ((1355271023, 1.80285))
    previousLines += ((1355271024, 1.80275))
    previousLines += ((1355271026, 1.80285))
    previousLines += ((1355271027, 1.80265))
    previousLines += ((1355271056, 1.80275))
    previousLines += ((1355271428, 1.80265))
    previousLines += ((1355271466, 1.80275))
    previousLines += ((1355271471, 1.80295))
    previousLines += ((1355271507, 1.80265))
    previousLines += ((1355271562, 1.80275))
    val result = TSeries.analyseAtTimeT(1355271588, 1.80295, 60, previousLines)._1.mkString
    assert(result === "1355271588 1.80295 2 3.60570 1.80275 1.80295")
  }
}