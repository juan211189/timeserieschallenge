# README #

Time series challenge

### How do I get set up? ###

* Create jar using the command:
sbt assembly

* The program requires two arguments: file path, rolling time window

*Execute the application using the following example:
scala -classpath "/path_to_jar/TimeSeriesChallenge-assembly-1.0.jar" fyber.challenge.one.TSeries /path_to_file/input_file.txt 60

### Assumptions ###

- The time series in the input file are sorted from the oldest to the most recent timestamp.
- The price ratios in the input do not contain more than 5 decimal digits, so this restriction do not have to be validated for the input, only for the calculations.
- The price ratios, which fit into Scala Double data type, are not very large numbers according to the sample input, which means the rolling sum of price ratios also fit into Scala Double data type. For this consideration, I also assume that the rolling window will never be that large so that the rolling sum cannot fit the Double data type.
- The rolling time window must be an integer value as in the given example.
- The input file contains only correct information in the format mentioned in the problem description.
- The timestamp limit is not inclusive. Please read the following example:

	If I get a timestamp1 = 60, then timestamp2 = 90 and then a timestamp3 = 120 with a window=60, the analysis at timestamp3 will only inlcude timestamp3 and timestamp2 because (120-60) = 60 is not less than the window. This means at the second 120 only the timestamps from 61 on can be included in the window analysis. 

